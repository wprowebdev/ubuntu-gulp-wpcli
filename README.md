# Wordpress Docker Image

Based on Ubuntu 18.04 LTS

**Includes**

- NodeJS LTS
- GulpJS
- PHP 7.2
- WP-CLI
- Composer